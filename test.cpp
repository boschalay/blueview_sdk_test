#include <bvt_sdk.h>
#include <iostream>

int main(int argc, char* argv[])
{
  /*
  std::cout << "Beginning" << std::endl;
  // BVTSonar sonar = BVTSonar_Create();
  /*
  BVTSonar_Open(sonar, "FILE", "/media/jep/Data/fls_datatsets/blueview_datasets/Airplane-sample.son");
  BVTHead head = NULL;
  BVTSonar_GetHead(sonar, 0, &head);
  BVTPing ping = NULL;
  BVTHead_GetPing(head, 0, &ping);
  BVTPing_Destroy(ping);
  BVTHead_Destroy(head);
  */
  // BVTSonar_Destroy(sonar);
  // std::cout << "End" << std::endl;

  std::cout << "Beginning" << std::endl;
  BVTSDK::Sonar sonar;
  sonar.Open("FILE", "/media/jep/Data/fls_datatsets/blueview_datasets/Airplane-sample.son");
  BVTSDK::Head head = sonar.GetHead(0);
  BVTSDK::Ping ping = head.GetPing(0);
  /* Do something useful with the ping here */
  std::cout << "End" << std::endl;

  return 0;
}
